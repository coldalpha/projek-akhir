<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index');
Route::get('/cari', 'HomeController@cari');
Route::get('/dashboard', 'PerpusController@index');
Route::get('/perpus', 'PerpusController@show');
Route::get('/perpus/cari', 'PerpusController@cari');
Route::get('/perpus/create', 'PerpusController@create');
Route::post('/perpus', 'PerpusController@store');
Route::get('/perpus/{perpus_id}', 'PerpusController@detail');
Route::get('/perpus/{perpus_id}/edit', 'PerpusController@edit');
Route::put('/perpus/{perpus_id}', 'PerpusController@update');
Route::delete('/perpus/{perpus_id}', 'PerpusController@destroy');
// Route::get('/register', 'AuthController@daftar');
// Route::post('/welcome', 'AuthController@submit');
// Route::get('/master', function () {
//     return view('layouts.master');
// });
// Route::get('/table', function () {
//     return view('layouts.table');
// });
// Route::get('/data-tables', function () {
//     return view('layouts.data-tables');
// });

// Route::get('/cast', 'CastController@index');
// Route::get('/cast/create', 'CastController@create');
// Route::post('/cast', 'CastController@store');
// Route::get('/cast/{cast_id}', 'CastController@show');
// Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Route::put('/cast/{cast_id}', 'CastController@update');
// Route::delete('/cast/{cast_id}', 'CastController@destroy');