@extends('layouts.master')
@section('title')
Tampil Data Buku
@endsection
@section('content')
<div class="card">
        <div class="card-header">
          <h3 class="card-title"><a href="/perpus/create" class="btn btn-primary my-2">Tambah</a></h3>
          
          <div class="card-tools">
            <div class="card-tools">
              <form action="/perpus/cari" method="GET">
              <div class="input-group input-group-lg my-2" style="width: 400px;">
                <input type="text" name="cari" class="form-control float-right" placeholder="Cari Judul Buku">
                <div class="input-group-append">
                  <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Judul</th>
                    <th scope="col">ISBN</th>
                    <th scope="col">Nomor Induk</th>
                    <th scope="col" >Pengarang</th>
                    <th scope="col" >Penerbit</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse ($perpus as $key=>$value)
                        <tr>
                            <td>{{$key+$perpus->firstItem()}}</th>
                            <td>{{$value->judul}}</td>
                            <td>{{$value->ISBN}}</td>
                            <td>{{$value->no_induk}}</td>
                            <td>{{$value->nama_pengarang}}</td>
                            <td>{{$value->nama_penerbit}}</td>
                            <td>
                                <form action="/perpus/{{$value->id}}" method="POST">
                                    <a href="/perpus/{{$value->id}}" class="btn btn-info">Show</a>
                                    <a href="/perpus/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr colspan="3">
                            <td>No data</td>
                        </tr>  
                    @endforelse              
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
          <div class="card-footer">
            <ul class="pagination pagination-sm my-2 float-right">
              {{ $perpus->links() }}
            </ul>
          </div>
      </div>
@endsection

