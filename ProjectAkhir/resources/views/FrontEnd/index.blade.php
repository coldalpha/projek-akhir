<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>PERPUSTAKAAN</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('admin/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('admin/dist/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition layout-top-nav">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
            <div class="container">
                <a href="/dashboard" class="navbar-brand">
                    <img src="{{asset('admin/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                    <span class="brand-text font-weight-light">PERPUSTAKAAN</span>
                </a>
                <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                
                <div class="collapse navbar-collapse order-3" id="navbarCollapse">
                </div>
            </div>
        </nav>
        <!-- /.navbar -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container">
                    <div class="row mb-2">
                        <div class="col">
                            <h1 class="m-0 text-dark"><strong>Semarakan Semangat Literasi Di Lingkungan Sekitar</strong></h1>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                    <div class="card-header">
                                        <div class="card-tools">
                                            <form action="/cari" method="GET">
                                            <div class="input-group input-group-sm" style="width: 400px;">
                                              <input type="text" name="cari" class="form-control float-right my-1" placeholder="Cari Buku Anda">
                                              <div class="input-group-append">
                                                <button type="submit" class="btn btn-default my-1"><i class="fas fa-search"></i></button>
                                              </div>
                                            </div>
                                            </form>
                                          </div>
                                            <ul class="pagination pagination-sm m-0 float-left">
                                              {{ $perpus->links() }}
                                            </ul>
                                    </div>
                                <div class="card-body table-responsive">
                                    <table class="table table-hover text-nowrap">
                                        <thead class="thead-light">
                                          <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Judul</th>
                                            <th scope="col">ISBN</th>
                                            <th scope="col">Nomor Induk</th>
                                            <th scope="col" >Pengarang</th>
                                            <th scope="col" >Penerbit</th>
                                            
                                          </tr>
                                        </thead>
                                        <tbody>
                                            
                                            @forelse ($perpus as $key=>$value)
                                                <tr>
                                                    <td>{{$key+$perpus->firstItem()}}</th>
                                                    <td>{{$value->judul}}</td>
                                                    <td>{{$value->ISBN}}</td>
                                                    <td>{{$value->no_induk}}</td>
                                                    <td>{{$value->nama_pengarang}}</td>
                                                    <td>{{$value->nama_penerbit}}</td>
                                                    
                                                </tr>
                                            @empty
                                                <tr colspan="3">
                                                    <td>No data</td>
                                                </tr>  
                                            @endforelse              
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col-md-6 -->
                        
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline">
                Anything you want
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('admin/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('admin/dist/js/demo.js')}}"></script>
</body>

</html>