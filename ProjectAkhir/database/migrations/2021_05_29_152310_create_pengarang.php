<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengarang', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_pengarang');
            $table->string('alamat_pengarang');
        });
        DB::table('pengarang')->insert([
            ['nama_pengarang' => 'Tien Tan Twu', 'alamat_pengarang' => 'Jl. Ahmad Yani, No. 52, Bandung Barat'],
            ['nama_pengarang' => 'Bagus Anggoro Sukma', 'alamat_pengarang' => 'Jl. Pegangsaan Utara, No. 1, Cimahi'],
            ['nama_pengarang' => 'Tri Buwono Saksono', 'alamat_pengarang' => 'Jl. Manuk Werit, No. 89, Bantul']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengarang');
    }
}