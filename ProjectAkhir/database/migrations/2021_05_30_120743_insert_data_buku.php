<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertDataBuku extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('buku')->insert([
            ['judul' => 'Kisah Tanah Jawa', 'isbn' => '0989823', 'no_induk' => '1425', 'pengarang_id' => '1', 'penerbit_id' => '1'],
            ['judul' => 'Tere Liye - Bumi', 'isbn' => '8979132', 'no_induk' => '8273', 'pengarang_id' => '2', 'penerbit_id' => '2'],
            ['judul' => 'Tanah Jahanam', 'isbn' => '1237824', 'no_induk' => '6283', 'pengarang_id' => '3', 'penerbit_id' => '3'],
            ['judul' => 'Tere Liye - Langit', 'isbn' => '6425657', 'no_induk' => '9683', 'pengarang_id' => '1', 'penerbit_id' => '2'],
            ['judul' => 'Kisah Upin dan Ipin', 'isbn' => '8512223', 'no_induk' => '7843', 'pengarang_id' => '1', 'penerbit_id' => '3'],
            ['judul' => 'Kisah Sang Kancil', 'isbn' => '8822342', 'no_induk' => '2345', 'pengarang_id' => '2', 'penerbit_id' => '1'],
            ['judul' => 'Satu Jalan Menuju Surga', 'isbn' => '345736', 'no_induk' => '1354', 'pengarang_id' => '2', 'penerbit_id' => '2'],
            ['judul' => 'Selangkah dari Neraka', 'isbn' => '6734536', 'no_induk' => '1222', 'pengarang_id' => '2', 'penerbit_id' => '3'],
            ['judul' => 'Tere Liye - Badai', 'isbn' => '8563456', 'no_induk' => '6634', 'pengarang_id' => '3', 'penerbit_id' => '1'],
            ['judul' => 'Sukma di dalam Raga', 'isbn' => '8996355', 'no_induk' => '8856', 'pengarang_id' => '3', 'penerbit_id' => '2'],
            ['judul' => 'Buku Jendela Ilmu', 'isbn' => '3465355', 'no_induk' => '9569', 'pengarang_id' => '3', 'penerbit_id' => '3'],
            ['judul' => 'Bersama dalam Duka', 'isbn' => '4453452', 'no_induk' => '8906', 'pengarang_id' => '1', 'penerbit_id' => '3'],
            ['judul' => 'Mencoba atau tidak sama sekali', 'isbn' => '890456', 'no_induk' => '4566', 'pengarang_id' => '2', 'penerbit_id' => '3'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}