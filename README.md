## Final Project

## Kelompok 18

## Anggota Kelompok

- Dhika Rizky Arianto
- Muhammad Aura Al Fataa

## Tema Project

PERPUSTAKAAN

## ERD

<p align="center"><a href="#" target="_blank"><img src="https://gitlab.com/coldalpha/sanbercode-fataa/-/blob/master/ERD/projekakhir.svg" width="400"></a></p>

## Link Video

- Link Demo Aplikasi : [Klik Me](https://youtu.be/zhLSvv5VJNg).
- Link Deploy : [Klik Me](https://youtu.be/zhLSvv5VJNg).
